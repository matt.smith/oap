For Students:
Students should use the left hand navigation menu of the web application to navigate through the different content of the CMS.
From the navigation menu, students can navigate through different subjects by touching each subject via touch screen.
Once a subject is selected, a list of pages for each subject will be displayed for the subject.
These pages will contain different information about each subject.
Students will also be able to view a list of upcoming courses that are going to be offered in the following semester.

For Admins:
Admins will need to login to the system by manually navigating to the login page in the address bar.
Once logged in, Admins will be brought to an Admin Hub.
This hub will allow the Admin to manage the system's database. An admin will be able to perform CRUD for the site's content, the admin accounts for the site, and the upcoming courses in the course planner section of the site.

In order for this system to work, you will need an AMP stack on your server (apache, mysql, php). You will also need to ensure that
an informe_db database is created on your server, with a databse user of csc440 and password CSC440student.

We recommend for windows systems to install XAMPP or WAMP. For Linux, we recommend installing LAMP. For MAC OSX, we reccomend MAMP.  