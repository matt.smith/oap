<?php
define("DB_SERVER", "localhost");
define("DB_USER", "informe_user");
define("DB_PASS", "informe_password");
define("DB_NAME", "informe_db");

// 1. Create a database connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
// Test if connection succeeded
if (mysqli_connect_errno()) {
    die("Database connection failed: " .
        mysqli_connect_error() . " (" . mysqli_connect_errno() . ")" );
}
