<?php

$errors = array();

function fieldname_as_text($fieldname) {
    $fieldname = str_replace("_", " ", $fieldname);
    $fieldname = ucfirst($fieldname);
    return $fieldname;
}

// * presence
// use trim() so empty spaces don't count
// use === to avoid false positives
// empty() would consider "0" to be empty
function has_presence($value) {
    return isset($value) && $value !== "";
}

function validate_presences($required_fields) {
  global $errors;
  foreach($required_fields as $field) {
    $value = trim($_POST[$field]);
  	if (!has_presence($value)) {
  		$errors[$field] = fieldname_as_text($field) . " can't be blank";
  	}
  }
}

function validate_account_info($required_fields, $username) {
    global $errors;
    foreach($required_fields as $field) {
        $value = trim($_POST[$field]);
        if (!has_presence($value)) {
            $errors[$field] = fieldname_as_text($field) . " can't be blank.";
        }
        else {
            if ($field == "username") {
                if (!(filter_var($value, FILTER_VALIDATE_EMAIL))) {
                    $errors[$field] = fieldname_as_text($field) . " must be valid (someone@somewhere.com).";
                }
                else {
                    //basically, this function is making sure that admin emails contain "@mymail.eku.edu"
                    if (empty($errors)) {
                        if (!strpos($username,'@mymail.eku.edu')) {
                            $errors[$username] = "Username must be your eku email '@mymail.eku.edu'";
                        }
                    }
                }
            }
        }
    }
}

//function eku_email_required($email) {
//    global $errors;
//    // if other errors exist, then that means that either the email was left blank, or something else was
//    // either way, this does not need to execute if other errors exists.
//
//    //basically, this function is making sure that admin emails contain "@aloha"
//    if (empty($errors)) {
//        if (!strpos($email,'@mymail.eku.edu')) {
//            $errors[$email] = "Username must be your eku email '@mymail.eku.edu'";
//        }
//    }
//}

// * string length
// max length
function has_max_length($value, $max) {
	return strlen($value) <= $max;
}

function validate_max_lengths($fields_with_max_lengths) {
	global $errors;
	// Expects an assoc. array
	foreach($fields_with_max_lengths as $field => $max) {
        $value = trim($_POST[$field]);
	  if (!has_max_length($value, $max)) {
	    $errors[$field] = fieldname_as_text($field) . " is too long";
	  }
	}
}

// * inclusion in a set
function has_inclusion_in($value, $set) {
    return in_array($value, $set);
}
