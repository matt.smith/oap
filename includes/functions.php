<?php

class administrators {
    // This is the class that will handle the functions needed for admins.
    // This class will be able to do the following:
    //      Find all the admins that are stored in the database
    //      Find an admin by their id, which will be the primary key in the admins table
    //      Find an admin by their username, currently this is not unique, hoping to fix it later

    function find_all_admins() {
        global $site;
        global $connection;

        $query  = "SELECT * ";
        $query .= "FROM informe_admins ";
        $query .= "ORDER BY username ASC";
        $admin_set = mysqli_query($connection, $query);
        $site ->confirm_query($admin_set);
        return $admin_set;
    }

    function find_admin_by_id($admin_id) {
        global $site;
        global $connection;
        $safe_admin_id = mysqli_real_escape_string($connection, $admin_id);

        $query  = "SELECT * ";
        $query .= "FROM informe_admins ";
        $query .= "WHERE id = {$safe_admin_id} ";
        $query .= "LIMIT 1";
        $admin_set = mysqli_query($connection, $query);
        $site -> confirm_query($admin_set);
        if ($admin = mysqli_fetch_assoc($admin_set)) {
            return $admin;
        }
        else {
            return null;
        }
    }

    function find_admin_by_username($admin_username) {
        global $site;
        global $connection;
        $safe_admin_username = mysqli_real_escape_string($connection, $admin_username);

        $query  = "SELECT * ";
        $query .= "FROM informe_admins ";
        $query .= "WHERE username = '{$safe_admin_username}' ";
        $query .= "LIMIT 1";
        $admin_set = mysqli_query($connection, $query);
        $site -> confirm_query($admin_set);
        if ($admin = mysqli_fetch_assoc($admin_set)) {
            return $admin;
        }
        else {
            return null;
        }
    }
}

class course_schedules {
    // This is the class that will handle the functions needed for admins to manage course schedules.
    // This class will be able to do the following:
    //      Find all the course schedules that are stored in the database
    //      Find a course schedule by their crn, which will be the primary key in the course_schedules table
    //      Find a course by their course_name

    function find_all_course_schedules() {
        global $site;
        global $connection;

        $query  = "SELECT * ";
        $query .= "FROM informe_course_schedules ";
        $query .= "ORDER BY course_subject ASC";
        $course_schedule_set = mysqli_query($connection, $query);
        $site ->confirm_query($course_schedule_set);
        return $course_schedule_set;
    }

    function find_course_schedule_by_crn($course_schedule_crn) {
        global $site;
        global $connection;
        $safe_course_schedule_crn = mysqli_real_escape_string($connection, $course_schedule_crn);

        $query  = "SELECT * ";
        $query .= "FROM informe_course_schedules ";
        $query .= "WHERE crn = {$safe_course_schedule_crn} ";
        $query .= "LIMIT 1";
        $course_schedule_set = mysqli_query($connection, $query);
        $site -> confirm_query($course_schedule_set);
        if ($course_schedule = mysqli_fetch_assoc($course_schedule_set)) {
            return $course_schedule;
        }
        else {
            return null;
        }
    }

    function find_course_schedule_by_course_name($course_name) {
        global $site;
        global $connection;
        $safe_course_name = mysqli_real_escape_string($connection, $course_name);

        $query  = "SELECT * ";
        $query .= "FROM informe_course_schedules ";
        $query .= "WHERE course_name = '{$safe_course_name}' ";
        $query .= "LIMIT 1";
        $course_schedule_set = mysqli_query($connection, $query);
        $site -> confirm_query($course_schedule_set);
        if ($course_schedule = mysqli_fetch_assoc($course_schedule_set)) {
            return $course_schedule;
        }
        else {
            return null;
        }
    }
}

class subjects {
    // This is the subjects class that will handle the functions needed for the subjects for PHP Sandbox
    // This class will be able to do the following:
    //      Find all the subjects that are stored in the database
    //      Find pages that are associated with each subject
    //      Find subjects by their id number, which is the primary key in the subjects table

    function find_all_subjects($public_area) {
//    if $public_area is TRUE, then the user is in the public area, and shouldn't be able to find ALL pages for a subject
//    users should only be able to find ALL pages for a subject, when in the admin area global $connection;
        global $site;
        global $connection;
        $query  = "SELECT * ";
        $query .= "FROM informe_subjects ";
        if ($public_area) {
            //if NOT in the admin area, only subject that are visible will appear for user
            $query .= "WHERE visible = 1 ";
        }

        $query .= "ORDER BY position ASC";
        $subject_set = mysqli_query($connection, $query);
        $site -> confirm_query($subject_set);
        return $subject_set;
    }

    function find_pages_for_subject($subject_id, $public_area) {
        // if $public_area is TRUE, then the user is in the public area, and shouldn't be able to find ALL pages for subject
        // users should only be able to find ALL pages for subject, when in the admin area
        global $site;
        global $connection;

        $safe_subject_id = mysqli_real_escape_string($connection, $subject_id);

        $query  = "SELECT * ";
        $query .= "FROM informe_pages ";
        $query .= "WHERE subject_id = {$safe_subject_id} ";
        if ($public_area) {
            //if NOT in the admin area, only subject that are visible will appear for user
            $query .= "AND visible = 1 ";
        }
        $query .= "ORDER BY position ASC";
        $page_set = mysqli_query($connection, $query);
        $site -> confirm_query($page_set);
        return $page_set;
    }

    function find_subject_by_id($subject_id, $public_area) {
        global $site;
        global $connection;

        $safe_subject_id = mysqli_real_escape_string($connection, $subject_id);

        $query  = "SELECT * ";
        $query .= "FROM informe_subjects ";
        $query .= "WHERE id = {$safe_subject_id} ";
        if ($public_area) {
            $query .= "AND visible = 1 ";
        }
        $query .= "LIMIT 1";
        $subject_set = mysqli_query($connection, $query);
        $site -> confirm_query($subject_set);
        if($subject = mysqli_fetch_assoc($subject_set)) {
            return $subject;
        }
        else {
            return null;
        }
    }
}

class pages {
    function find_page_by_id($page_id, $public_area) {
        // if $public_area is true, then the user is in the public area, and can only find the pages that have visible = 1
        // if the user is not in the public area, then they can find any page, regardless of if visible = 1
        global $site;
        global $connection;

        $safe_page_id = mysqli_real_escape_string($connection, $page_id);

        $query  = "SELECT * ";
        $query .= "FROM informe_pages ";
        $query .= "WHERE id = {$safe_page_id} ";
        if ($public_area) {
            $query .= "AND visible = 1 ";
        }
        $query .= "LIMIT 1";
        $page_set = mysqli_query($connection, $query);
        $site -> confirm_query($page_set);
        if($page = mysqli_fetch_assoc($page_set)) {
            return $page;
        } else {
            return null;
        }
    }

    function find_default_page_for_subject($subject_id) {
        global $subjects;
        $page_set = $subjects -> find_pages_for_subject($subject_id, $public_area = True);
        if ($first_page = mysqli_fetch_assoc($page_set)) {
            return $first_page;
        }
        else {
            return null;
        }
    }

    function find_selected_page($public_area) {
        // if $public_area is false, then the user is in the public area, and so the bold subject w/ page will apply
        // only want those for users, it helps them out. admin won't need this feature, he/she knows what they're doing
        global $subjects;
        global $pages;
        global $current_subject;
        global $current_page;

        if (isset($_GET["subject"])) {
            $current_subject = $subjects -> find_subject_by_id($_GET["subject"], $public_area);
            if ($current_subject and $public_area) {
                $current_page = $pages -> find_default_page_for_subject($current_subject["id"]);
            } else{
                $current_page = null;
            }
        }
        else if (isset($_GET["page"])) {
            $current_subject = null;
            $current_page = $pages -> find_page_by_id($_GET["page"], $public_area);
        } else {
            $current_subject = null;
            $current_page = null;
        }
    }
}

class site {
    function redirect_to($new_location) {
        header("Location: " . $new_location);
        exit;
    }

    function mysql_prep($string) {
        global $connection;

        $escaped_string = mysqli_real_escape_string($connection, $string);
        return $escaped_string;
    }

    function confirm_query($result_set) {
        if (!$result_set) {
            die("Database query failed.");
        }
    }

    function form_errors($errors = array()) {
        $output = "";
        if (!empty($errors)) {
            $output .= "<div class=\"error\">";
            $output .= "Please fix the following errors:";
            $output .= "<ul>";
            foreach ($errors as $key => $error) {
                $output .= "<li>";
                $output .= htmlentities($error);
                $output .= "</li>";
            }
            $output .= "</ul>";
            $output .= "</div>";
        }
        return $output;
    }

    function navigation($subject_array, $page_array) {
        // navigation takes 2 arguments
        // - the current subject array or null
        // - the current page array or null

        global $subjects;

        $output = "<ul class=\"subjects\">";
        // False is indicating that the user is in the ADMIN area, restrictions ARE NOT necessary
        $subject_set = $subjects -> find_all_subjects($public_area = False);
        while($subject = mysqli_fetch_assoc($subject_set)) {
            $output .= "<li";
            if ($subject_array && $subject["id"] == $subject_array["id"]) {
                $output .= " class=\"selected\"";
            }
            $output .= ">";
            $output .= "<a href=\"manage_content.php?subject=";
            $output .= urlencode($subject["id"]);
            $output .= "\">";
            $output .= htmlentities($subject["menu_name"]);
            $output .= "</a>";

            if ($subject_array["id"] == $subject["id"] ||
                $page_array["subject_id"] == $subject["id"]) {
                // False is indicating that the user is in the ADMIN area, restrictions ARE necessary
                $page_set = $subjects -> find_pages_for_subject($subject["id"], $public_area = False);
                $output .= "<ul class=\"pages\">";
                while($page = mysqli_fetch_assoc($page_set)) {
                    $output .= "<li";
                    if ($page_array && $page["id"] == $page_array["id"]) {
                        $output .= " class=\"selected\"";
                    }
                    $output .= ">";
                    $output .= "<a href=\"manage_content.php?page=";
                    $output .= urlencode($page["id"]);
                    $output .= "\">";
                    $output .= htmlentities($page["menu_name"]);
                    $output .= "</a></li>";
                }
                $output .= "</ul>";
                mysqli_free_result($page_set);
            }
            $output .= "</li>"; // end of the subject li
        }
        mysqli_free_result($subject_set);
        $output .= "</ul>";
        return $output;
    }

    function public_navigation($subject_array, $page_array) {
        // navigation takes 2 arguments
        // - the current subject array or null
        // - the current page array or null

        global $subjects;

        $output = "<ul class=\"subjects\">";
        // True is indicating that the user is in the PUBLIC area, restrictions ARE necessary
        $subject_set = $subjects -> find_all_subjects($public_area = True);
        while($subject = mysqli_fetch_assoc($subject_set)) {
            $output .= "<li";
            if ($subject_array && $subject["id"] == $subject_array["id"]) {
                $output .= " class=\"selected\"";
            }
            $output .= ">";
            $output .= "<a href=\"index.php?subject=";
            $output .= urlencode($subject["id"]);
            $output .= "\">";
            $output .= htmlentities($subject["menu_name"]);
            $output .= "</a>";

            if ($subject_array["id"] == $subject["id"] ||
                $page_array["subject_id"] == $subject["id"]) {
                // True is indicating that the user is in the PUBLIC area, restrictions ARE necessary
				$page_set = $subjects -> find_pages_for_subject($subject["id"], $public_area = True);
                $output .= "<ul class=\"pages\">";
                while($page = mysqli_fetch_assoc($page_set)) {
                    $output .= "<li";
                    if ($page_array && $page["id"] == $page_array["id"]) {
                        $output .= " class=\"selected\"";
                    }
                    $output .= ">";
                    $output .= "<a href=\"index.php?page=";
                    $output .= urlencode($page["id"]);
                    $output .= "\">";
                    $output .= htmlentities($page["menu_name"]);
                    $output .= "</a></li>";
                }
                $output .= "</ul>";
                mysqli_free_result($page_set);
            }
            $output .= "</li>"; // end of the subject li
        }
        mysqli_free_result($subject_set);
        $output .= "</ul>";
        return $output;
    }

    function password_encryption($password) {
        $hash_format = "$2y$10$";           // Tells PHP to use Blowfish with a "cost" of 10
        $salt_length = 22; 					// Blowfish salts should be 22-characters or more
        $salt = $this -> generate_salt($salt_length);
        $format_and_salt = $hash_format . $salt;
        $hash = crypt($password, $format_and_salt);

        return $hash;
    }

    function generate_salt($salt_length) {
        // Not 100% unique, not 100% random, but good enough for a salt
        // MD5 returns 32 characters
        $unique_random_string = md5(uniqid(mt_rand(), True));

        // Valid characters for a salt are [a-zA-Z0-9./]
        $base64_string = base64_encode($unique_random_string);

        // But not '+' which is valid in base64 encoding
        $modified_base64_string = str_replace('+', '.', $base64_string);

        // Truncate string to the correct length
        $salt = substr($modified_base64_string, 0, $salt_length);

        return $salt;
    }

    function password_checker($password, $existing_hash) {
        $hash = crypt($password, $existing_hash);
        // if these are exactly equal, then match was found, the existing hash will contains format_and_salt at the start
        if ($hash === $existing_hash) {
            return True;
        }
        else {
            return False;
        }
    }

    function attempt_to_login($username, $password) {
        // compare to see if database contains the username and password
        // 1. find the user
        // 2. compare password

        global $administrators;

        $admin = $administrators -> find_admin_by_username($username);         // step 1 listed above

        if ($admin) {
            // the admin was found, now we will check for password
            if ($this -> password_checker($password, $admin["hashed_password"])) {       // step 2 listed above
                // the password matches
                return $admin;
            }
            else {
                // password does not match
                return False;
            }
        }
        else {
            // the admin was not found
            return False;
        }
    }

    function logged_in() {
        // checks to see if user is logged in as an admin, used in some areas of site that gives user admin abilities
        // if user isn't an admin, then they do not get to access these pages

        // might possibly incorporate this into my context for public and admin areas, instead of passing straight up
        // true/false values into the functions

        return isset($_SESSION["admin_id"]);
    }

    function logged_in_confirmation() {
        if (!$this -> logged_in()) {
            $this -> redirect_to("index.php");
        }
    }
}

$administrators = new administrators();
$course_schedules = new course_schedules();
$subjects = new subjects();
$pages = new pages();
$site = new site();
