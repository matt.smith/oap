<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
$current_subject = $subjects -> find_subject_by_id($_GET["subject"], $public_area = False);
if (!$current_subject) {
    // subject ID was missing or invalid or
    // subject couldn't be found in database
    $site -> redirect_to("manage_content.php");
}
// False is indicating that the user is in the ADMIN area, restrictions ARE NOT necessary
$pages_set = $subjects -> find_pages_for_subject($current_subject["id"], $public_area = False);
if (mysqli_num_rows($pages_set) > 0) {
    $_SESSION["message"] = "Can't delete a subject with pages.";
    $site -> redirect_to("manage_content.php?subject={$current_subject["id"]}");
}

$id = $current_subject["id"];
$query = "DELETE FROM informe_subjects WHERE id = {$id} LIMIT 1";
$result = mysqli_query($connection, $query);

if ($result && mysqli_affected_rows($connection) == 1) {
    // Success
    $_SESSION["message"] = "Subject deleted.";
    $site -> redirect_to("manage_content.php");
}
else {
    // Failure
    $_SESSION["message"] = "Subject deletion failed.";
    $site -> redirect_to("manage_content.php?subject={$id}");
}
