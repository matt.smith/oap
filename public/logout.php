<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php

// if user is not logged in currently, then they obviouly have no need to logout.
$site -> logged_in_confirmation();

// simple logout by setting the username to null so the session will know the user has logged out
session_start();
$_SESSION["username"] = null;
if (isset($_SESSION["admin_id"])) {
    $_SESSION["admin_id"] = null;
}

$_SESSION["message"] = "You have logged out.";
$site -> redirect_to("login.php");

?>
