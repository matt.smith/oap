<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
$course_schedule = $course_schedules -> find_course_schedule_by_crn($_GET["crn"]);

if (!$course_schedule) {
    // course_schedule crn is missing, or it is invalid
    $site -> redirect_to("manage_course_schedules.php");
}

$crn = $course_schedule["crn"];
$query = "DELETE FROM informe_course_schedules WHERE crn = {$crn} LIMIT 1";
$result = mysqli_query($connection, $query);

if ($result && mysqli_affected_rows($connection) == 1) {
    // Success
    $_SESSION["message"] = "Course was deleted.";
    $site -> redirect_to("manage_course_schedules.php");
}
else {
    // Failure
    $_SESSION["message"] = "Course deletion failed.";
    $site -> redirect_to("manage_course_schedules.php");
}

?>
