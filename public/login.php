<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if a user is already logged in as an admin, then they do not need to login again.
// they will be directed to admin.php.
if (isset($_SESSION["admin_id"])) {
    $site -> redirect_to("admin.php");
}
?>

<?php
// default these to null to avoid errors
$username = "";
$password = "";

if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $required_fields = array("username", "password");
    validate_presences($required_fields);

    if (empty($errors)) {
        // Attempt to login

        $username = $_POST["username"];
        $password = $_POST["password"];

        $result = $site -> attempt_to_login($username, $password);

        if ($result = $administrators -> find_admin_by_username($username)) {
            // Success
            // The user has logged in, the session needs to keep this information for site navigation
            // Some parts of site will require to be logged in, so if this isnt saved certain areas are unavailable
            $_SESSION["admin_id"] = $result["id"];
            $_SESSION["username"] = $result["username"];
            $site -> redirect_to("admin.php");
        }
        else {
            // Failure
            $_SESSION["message"] = "Username/password not found.";
        }
    }
}
else {
    // This is probably a GET request

} // end: if (isset($_POST['submit']))
?>

<?php //$layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>
<div id="main">
    <div id="navigation">
        <br />
        <a href="index.php">&laquo; Main menu</a>
        <br />
    </div>
    <div id="page">
        <?php echo message(); ?>
        <?php echo $site -> form_errors($errors); ?>

        <h2>Login</h2>
        <form action="login.php" method="post">
            <p>Username:
<!--                this will allow the user to see what username they have entered when they logged in-->
<!--                if they have not attempted a login yet, this will be blank-->
                <input type="text" name="username" value="<?php echo htmlentities($username); ?>" />
            </p>
            <p>Password:
                <input type="password" name="password" value="" />
            </p>
            <input type="submit" name="submit" value="Submit" />
        </form>
        <br />
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
