<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php $course_schedule_set = $course_schedules -> find_all_course_schedules();   // finds all the course_schedules stored in the database ?>

<?php $layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Admin menu</a>
        <br />

        <br />
        <a href="logout.php">Click here to log out</a>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <h2>Manage Course Schedules</h2>
        <table>
            <tr>
                <th style = "text-align: left; width: 150px;">CRN</th>
                <th style = "text-align: left; width: 150px;">Course Subject</th>
                <th style = "text-align: left; width: 150px;">Course Number</th>
                <th style = "text-align: left; width: 150px;">Course Name</th>
                <th style = "text-align: left; width: 150px;">Days Met</th>
                <th style = "text-align: left; width: 150px;">Times Met</th>
                <th colspan = "2" style = "text-align: left;">Actions</th>
            </tr>
            <?php while ($course_schedule = mysqli_fetch_assoc($course_schedule_set)) { ?>
                <tr>
                    <td>
                        <?php echo htmlentities($course_schedule["crn"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["course_subject"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["course_number"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["course_name"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["days_met"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["times_met"]); ?>
                    </td>
                    <td>
                        <a href = "edit_course_schedule.php?crn=<?php echo urlencode($course_schedule["crn"]); ?>">
                            Edit
                        </a>
                    </td>
                    <td>
                        <a href = "delete_course_schedule.php?crn=<?php echo urlencode($course_schedule["crn"]); ?>"
                           onclick="return confirm('Are you sure?');">Delete
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <br />
        <a href="new_course_schedule.php">+ Add a new course</a>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
