<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php $pages -> find_selected_page($public_area = False); ?>

<?php
  // Unlike new_page.php, we don't need a page_id to be sent
  // We already have it stored in pages.subject_id.
  if (!$current_page) {
    // page ID was missing or invalid or
    // page couldn't be found in database
      $site -> redirect_to("manage_content.php");
  }
?>

<?php
if (isset($_POST['submit'])) {
    // Process the form

    $id = $current_page["id"];
    $menu_name = $site -> mysql_prep($_POST["menu_name"]);
    $position = (int) $_POST["position"];
    $visible = (int) $_POST["visible"];
    $content = $site -> mysql_prep($_POST["content"]);

    // validations
    $required_fields = array("menu_name", "position", "visible", "content");
    validate_presences($required_fields);

    $fields_with_max_lengths = array("menu_name" => 255);
    validate_max_lengths($fields_with_max_lengths);
    if (empty($errors)) {
        // Perform Update

        $query  = "UPDATE informe_pages SET ";
        $query .= "menu_name = '{$menu_name}', ";
        $query .= "position = {$position}, ";
        $query .= "visible = {$visible}, ";
        $query .= "content = '{$content}' ";
        $query .= "WHERE id = {$id} ";
        $query .= "LIMIT 1";
        $result = mysqli_query($connection, $query);

        if ($result && mysqli_affected_rows($connection) == 1) {
            // Success
            $_SESSION["message"] = "Page updated.";
            $site -> redirect_to("manage_content.php?page={$id}");
        }
        else {
            // Failure
            $_SESSION["message"] = "Page update failed.";
        }
    }
}
else {
  // This is probably a GET request
  
} // end: if (isset($_POST['submit']))

?>

<?php $layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
  <div id="navigation">
      <br />
      <a href="admin.php">&laquo; Admin menu</a>
      <br />

      <?php echo $site -> navigation($current_subject, $current_page); ?>

      <br />
      <a href="logout.php">Click here to log out</a>
  </div>
  <div id="page">
    <?php echo message(); ?>
    <?php echo $site -> form_errors($errors); ?>
    
    <h2>Edit Page: <?php echo htmlentities($current_page["menu_name"]); ?></h2>
    <form action="edit_page.php?page=<?php echo urlencode($current_page["id"]); ?>" method="post">
      <p>Menu name:
        <input type="text" name="menu_name" value="<?php echo htmlentities($current_page["menu_name"]); ?>" />
      </p>
      <p>Position:
        <select name="position">
        <?php
        // False is indicating that the user is in the ADMIN area, restrictions ARE NOT necessary
        $page_set = $subjects -> find_pages_for_subject($current_page["subject_id"], $public_area = False);
        $page_count = mysqli_num_rows($page_set);
        for($count=1; $count <= $page_count; $count++) {
            echo "<option value=\"{$count}\"";
            if ($current_page["position"] == $count) {
                echo " selected";
            }
            echo ">{$count}</option>";
        }
        ?>
        </select>
      </p>
      <p>Visible:
        <input type="radio" name="visible" value="0"
            <?php if ($current_page["visible"] == 0) { echo "checked"; } ?> /> No
        &nbsp;
        <input type="radio" name="visible" value="1"
            <?php if ($current_page["visible"] == 1) { echo "checked"; } ?>/> Yes
      </p>
      <p>Content:<br />
        <textarea name="content" rows="20" cols="80">
            <?php echo htmlentities($current_page["content"]); ?>
        </textarea>
      </p>
      <input type="submit" name="submit" value="Edit Page" />
    </form>
    <br />
    <a href="manage_content.php?page=<?php echo urlencode($current_page["id"]); ?>">Cancel</a>
    &nbsp;
    &nbsp;
    <a href="delete_page.php?page=<?php echo urlencode($current_page["id"]); ?>"
       onclick="return confirm('Are you sure?');"> Delete page
    </a>
    
  </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
