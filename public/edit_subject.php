<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php $pages -> find_selected_page($public_area = False); ?>

<?php
	if (!$current_subject) {
		// subject ID was missing or invalid or 
		// subject couldn't be found in database
        $site -> redirect_to("manage_content.php");
	}
?>

<?php
if (isset($_POST['submit'])) {
	// Process the form
	
	// validations
	$required_fields = array("menu_name", "position", "visible");
	validate_presences($required_fields);
	
	$fields_with_max_lengths = array("menu_name" => 255);
	validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
		
		// Perform Update

		$id = $current_subject["id"];
		$menu_name = $site -> mysql_prep($_POST["menu_name"]);
		$position = (int) $_POST["position"];
		$visible = (int) $_POST["visible"];
	
		$query  = "UPDATE informe_subjects SET ";
		$query .= "menu_name = '{$menu_name}', ";
		$query .= "position = {$position}, ";
		$query .= "visible = {$visible} ";
		$query .= "WHERE id = {$id} ";
		$query .= "LIMIT 1";
		$result = mysqli_query($connection, $query);

		if ($result && mysqli_affected_rows($connection) >= 0) {
			// Success
			$_SESSION["message"] = "Subject updated.";
            $site -> redirect_to("manage_content.php");
		} else {
			// Failure
			$message = "Subject update failed.";
		}
	
	}
}
else {
	// This is probably a GET request
	
} // end: if (isset($_POST['submit']))

?>

<?php $layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
  <div id="navigation">
      <br />
      <a href="admin.php">&laquo; Admin menu</a>
      <br />

      <?php echo $site -> navigation($current_subject, $current_page); ?>

      <br />
      <a href="logout.php">Click here to log out</a>
  </div>
  <div id="page">
		<?php // $message is just a variable, doesn't use the SESSION
			if (!empty($message)) {
				echo "<div class=\"message\">" . htmlentities($message) . "</div>";
			}
		?>
		<?php echo $site -> form_errors($errors); ?>
		
		<h2>Edit Subject: <?php echo htmlentities($current_subject["menu_name"]); ?></h2>
		<form action="edit_subject.php?subject=<?php echo urlencode($current_subject["id"]); ?>" method="post">
		  <p>Menu name:
		    <input type="text" name="menu_name" value="<?php echo htmlentities($current_subject["menu_name"]); ?>" />
		  </p>
		  <p>Position:
		    <select name="position">
				<?php
				// False is indicating that the user is in the ADMIN area, restrictions ARE NOT necessary
				$subject_set = $subjects -> find_all_subjects($public_area = False);
				$subject_count = mysqli_num_rows($subject_set);
				for($count=1; $count <= $subject_count; $count++) {
                    echo "<option value=\"{$count}\"";
                    if ($current_subject["position"] == $count) {
                        echo " selected";
                    }
                    echo ">{$count}</option>";
				}
				?>
		    </select>
		  </p>
		  <p>Visible:
		    <input type="radio" name="visible" value="0"
                <?php if ($current_subject["visible"] == 0) { echo "checked"; } ?> /> No
		    &nbsp;
		    <input type="radio" name="visible" value="1"
                <?php if ($current_subject["visible"] == 1) { echo "checked"; } ?>/> Yes
		  </p>
		  <input type="submit" name="submit" value="Edit Subject" />
		</form>
		<br />
		<a href="manage_content.php">Cancel</a>
		&nbsp;
		&nbsp;
		<a href="delete_subject.php?subject=<?php echo urlencode($current_subject["id"]); ?>"
           onclick="return confirm('Are you sure?');"> Delete subject
        </a>
		
	</div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
