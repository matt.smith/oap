<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php $admin_set = $administrators -> find_all_admins();   // finds all the admins stored in the database ?>

<?php $layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Admin menu</a>
        <br />

        <br />
        <a href="logout.php">Click here to log out</a>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <h2>Manage Admins</h2>
        <table>
            <tr>
                <th style = "text-align: left; width: 200px;">Username</th>
                <th style = "text-align: left; width: 200px;">Admin ID</th>
                <th colspan = "2" style = "text-align: left;">Actions</th>
            </tr>
            <?php while ($admin = mysqli_fetch_assoc($admin_set)) { ?>
            <tr>
                <td>
                    <?php echo htmlentities($admin["username"]); ?>
                </td>
                <td>
                    <?php echo htmlentities($admin["id"]); ?>
                </td>
                <td>
                    <a href = "edit_admin.php?id=<?php echo urlencode($admin["id"]); ?>">
                        Edit
                    </a>
                </td>
                <td>
                    <a href = "delete_admin.php?id=<?php echo urlencode($admin["id"]); ?>"
                       onclick="return confirm('Are you sure?');">Delete
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
        <br />
        <a href="new_admin.php">+ Add a new Admin</a>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
