<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $required_fields = array("crn", "course_subject", "course_number", "course_name", "days_met", "times_met");
    validate_presences($required_fields);

    $fields_with_max_lengths = array("crn" => 10,"course_subject" => 5, "course_number" => 5, "course_name" => 255,
        "days_met" => 15, "times_met" => 25);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
        // Perform Create

        $crn = $site -> mysql_prep($_POST["crn"]);
        $course_subject = $site -> mysql_prep($_POST["course_subject"]);
        $course_number = strtoupper($site -> mysql_prep($_POST["course_number"]));
        $course_name = $site -> mysql_prep($_POST["course_name"]);
        $days_met = $site -> mysql_prep($_POST["days_met"]);
        $times_met = $site -> mysql_prep($_POST["times_met"]);

        $query  = "INSERT INTO informe_course_schedules (";
        $query .= "  crn, course_subject, course_number, course_name, days_met, times_met";
        $query .= ") VALUES (";
        $query .= "  '{$crn}', '{$course_subject}', '{$course_number}', '{$course_name}', '{$days_met}', '{$times_met}'";
        $query .= ")";
        $result = mysqli_query($connection, $query);

        if ($result) {
            // Success
            $_SESSION["message"] = "A new Course Schedule has been created.";
            $site -> redirect_to("manage_course_schedules.php");
        }
        else {
            // Failure
//            $_SESSION["message"] = "Failed to create a Course Schedule.";
            $_SESSION["message"] = $query;
        }
    }
}
else {
    // This is probably a GET request

} // end: if (isset($_POST['submit']))
?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>
<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Admin menu</a>
        <br />

        <br />
        <a href="logout.php">Click here to log out</a>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <?php echo $site -> form_errors($errors); ?>

        <h2>Create Course Schedule</h2>
        <form action="new_course_schedule.php" method="post">
            <p>CRN:
                <input type="text" name="crn" value="" />
            </p>
            <p>Course Subject:
                <input type="text" name="course_subject" value="" />
            </p>
            <p>Course Number:
                <input type="text" name="course_number" value="" />
            </p>
            <p>Course Name:
                <input type="text" name="course_name" value="" />
            </p>
            <p>Days Met:
                <input type="text" name="days_met" value="" />
            </p>
            <p>Times Met:
                <input type="text" name="times_met" value="" />
            </p>
            <input type="submit" name="submit" value="Create New Course Schedule" />

        </form>
        <br />
        <a href="manage_course_schedules.php">Cancel</a>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
