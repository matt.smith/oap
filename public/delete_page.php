<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
$current_page = $pages -> find_page_by_id($_GET["page"], $public_area = False);
if (!$current_page) {
    // page ID was missing or invalid or
    // page couldn't be found in database
    $site -> redirect_to("manage_content.php");
}

$id = $current_page["id"];
$query = "DELETE FROM informe_pages WHERE id = {$id} LIMIT 1";      // ensure only one page gets deleted here using LIMIT
$result = mysqli_query($connection, $query);

if ($result && mysqli_affected_rows($connection) == 1) {
    // Success
    $_SESSION["message"] = "Page deleted.";
    $site -> redirect_to("manage_content.php");
}
else {
    // Failure
    $_SESSION["message"] = "Page deletion failed.";
    $site -> redirect_to("manage_content.php?page={$id}");
}
