<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
$admin = $administrators -> find_admin_by_id($_GET["id"]);

if (!$admin) {
    // admin ID was missing or invalid or
    // admin couldn't be found in database
    $site -> redirect_to("manage_admins.php");
}
?>

<?php
if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $required_fields = array("username", "password");
    validate_account_info($required_fields, strtolower($site -> mysql_prep($_POST["username"])));

    $fields_with_max_lengths = array("username" => 30);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {

        // Perform Update

        $id = $admin["id"];
        $username = $site -> mysql_prep($_POST["username"]);
        $hashed_password = $site -> password_encryption($_POST["password"]);

        $query  = "UPDATE informe_admins SET ";
        $query .= "username = '{$username}', ";
        $query .= "hashed_password = '{$hashed_password}' ";
        $query .= "WHERE id = {$id} ";
        $query .= "LIMIT 1";
        $result = mysqli_query($connection, $query);

        if ($result && mysqli_affected_rows($connection) == 1) {
            // Success
            $_SESSION["message"] = "Admin {$username} has been updated.";
            $site -> redirect_to("manage_admins.php");
        }
        else {
            // Failure
            $_SESSION["message"] = "Admin update failed.";
        }

    }
}
else {
    // This is probably a GET request

} // end: if (isset($_POST['submit']))

?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Admin menu</a>
        <br />

        <br />
        <a href="logout.php">Click here to log out</a>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <?php echo $site -> form_errors($errors); ?>

        <h2>Edit Admin: <?php echo htmlentities($admin["username"]); ?></h2>
        <form action="edit_admin.php?id=<?php echo urlencode($admin["id"]); ?>" method="post">
            <p>Username:
                <input type="text" name="username" value="<?php echo htmlentities($admin["username"]); ?>" />
            </p>
            <p>Password:
                <input type="password" name="password" value="" />
            </p>
            <input type="submit" name="submit" value="Edit Admin" />
        </form>
        <br />
        <a href="manage_admins.php">Cancel</a>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
