<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
$course_schedule = $course_schedules -> find_course_schedule_by_crn($_GET["crn"]);

if (!$course_schedule) {
    // course_schedule crn was missing or invalid or
    // course_schedule couldn't be found in database
    $site -> redirect_to("manage_course_schedules.php");
}
?>

<?php
if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $required_fields = array("course_subject", "course_number", "course_name", "days_met", "times_met");
    validate_presences($required_fields);

    $fields_with_max_lengths = array("course_subject" => 5, "course_number" => 5, "course_name" => 255, "days_met" => 15,
        "times_met" => 25);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {

        // Perform Update

        $crn = $course_schedule["crn"];
        $course_subject = $site -> mysql_prep($_POST["course_subject"]);
        $course_number = strtoupper($site -> mysql_prep($_POST["course_number"]));
        $course_name = $site -> mysql_prep($_POST["course_name"]);
        $days_met = $site -> mysql_prep($_POST["days_met"]);
        $times_met = $site -> mysql_prep($_POST["times_met"]);

        $query  = "UPDATE informe_course_schedules SET ";
        $query .= "course_subject = '{$course_subject}', ";
        $query .= "course_number = '{$course_number}', ";
        $query .= "course_name = '{$course_name}', ";
        $query .= "days_met = '{$days_met}', ";
        $query .= "times_met = '{$times_met}' ";
        $query .= "WHERE crn = {$crn} ";
        $query .= "LIMIT 1";
        $result = mysqli_query($connection, $query);

        if ($result && mysqli_affected_rows($connection) == 1) {
            // Success
            $_SESSION["message"] = "Course Schedule for {$course_subject} {$course_number} has been updated.";
            $site -> redirect_to("manage_course_schedules.php");
        }
        else {
            // Failure
            $_SESSION["message"] = "Course Schedule failed to update.";
        }
    }
}
else {
    // This is probably a GET request

} // end: if (isset($_POST['submit']))

?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Admin menu</a>
        <br />

        <br />
        <a href="logout.php">Click here to log out</a>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <?php echo $site -> form_errors($errors); ?>

        <h2>Edit Course Schedule: <?php echo htmlentities($course_schedule["course_subject"] .
                " " . $course_schedule["course_number"]); ?>
        </h2>
        <form action="edit_course_schedule.php?crn=<?php echo urlencode($course_schedule["crn"]); ?>" method="post">
            <p>Course Subject:
                <input type="text" name="course_subject" value="<?php echo htmlentities($course_schedule["course_subject"]); ?>" />
            </p>
            <p>Course Number:
                <input type="text" name="course_number" value="<?php echo htmlentities($course_schedule["course_number"]); ?>" />
            </p>
            <p>Course Name:
                <input type="text" name="course_name" value="<?php echo htmlentities($course_schedule["course_name"]); ?>" />
            </p>
            <p>Days Met:
                <input type="text" name="days_met" value="<?php echo htmlentities($course_schedule["days_met"]); ?>" />
            </p>
            <p>Times Met:
                <input type="text" name="times_met" value="<?php echo htmlentities($course_schedule["times_met"]); ?>" />
            </p>
            <input type="submit" name="submit" value="Edit Course Schedule" />
        </form>
        <br />
        <a href="manage_course_schedules.php">Cancel</a>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
