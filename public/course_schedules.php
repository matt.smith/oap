<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php $course_schedule_set = $course_schedules -> find_all_course_schedules();   // finds all the course_schedules stored in the database ?>

<?php //$layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="index.php">&laquo; Main menu</a>
        <br />

        <?php if (isset($_SESSION["admin_id"])): ?>
            <br />
            <a href="logout.php">Click here to log out</a>
            <br />
        <?php endif; ?>

    </div>
    <div id="page">
        <?php echo message(); ?>
        <h2>CSC Course Schedule For Upcoming Semester</h2>
        <table>
            <tr>
                <th style = "text-align: left; width: 150px;">CRN</th>
                <th style = "text-align: left; width: 150px;">Course Subject</th>
                <th style = "text-align: left; width: 150px;">Course Number</th>
                <th style = "text-align: left; width: 150px;">Course Name</th>
                <th style = "text-align: left; width: 150px;">Days Met</th>
                <th style = "text-align: left; width: 150px;">Times Met</th>
            </tr>
            <?php while ($course_schedule = mysqli_fetch_assoc($course_schedule_set)) { ?>
                <tr>
                    <td>
                        <?php echo htmlentities($course_schedule["crn"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["course_subject"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["course_number"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["course_name"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["days_met"]); ?>
                    </td>
                    <td>
                        <?php echo htmlentities($course_schedule["times_met"]); ?>
                    </td>
                    <td>
                </tr>
            <?php } ?>
        </table>
        <br />
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
