<?php
// default page for all the public side
// this is the default page that servers will serve up, it will be very similar to the manage_content.php
?>

<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// below code is not necessary

// this will alter the header, if the user is in the public area of the site.
// this is needed, even though I'm not changing the title name to "PHP Sandbox Public"
// In my header.php file, the variable $layout_context would be undefined without this, causing errors.
// $layout_context = "public"
;?>

<?php include("../includes/layouts/header.php"); ?>
<?php $pages -> find_selected_page($public_area = True); ?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="index.php">What is InforMe?</a>
        <br />

        <?php echo $site -> public_navigation($current_subject, $current_page); ?>

        <br />
        <a href="course_schedules.php">InforMe Course Planner</a>
        <br />

        <?php if (isset($_SESSION["admin_id"])): ?>
            <br />
            <a href="admin.php">Admin menu &raquo</a>
            <br />

            <br />
            <a href="logout.php">Click here to log out</a>
        <?php endif; ?>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <?php if ($current_page): ?>
            <h2><?php echo htmlentities($current_page["menu_name"]); ?></h2>
            <?php echo nl2br(htmlentities($current_page["content"])); ?>
        <?php else: ?>
            <h2>This is InforMe's index page!</h2>
            <p>Welcome to the InforMe home page!</p>
            <p>We hope you find this information system useful.</p>
            <p>Navigate through the list of different options on the left hand side.</p>
            <p>Select a subject by simply touching it, and a list of pages for the subject will appear. </p>
            <p>To find out information about your selected subjected, simply touch any of the pages.</p>
            <p>
                If you would like to see what courses are available next semester for a specific degree option,
                click the "InforMe Course Planner" on the left hand side.
            </p>

            <br />
            
            <p>Be sure to click the "What is InforMe?" link if you need help with using the site.</p>
        <?php endif; ?>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
