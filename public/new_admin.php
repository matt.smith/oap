<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $required_fields = array("username", "password");
    validate_account_info($required_fields, strtolower($site -> mysql_prep($_POST["username"])));

    $fields_with_max_lengths = array("username" => 30);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
        // Perform Create

        $username = $site -> mysql_prep($_POST["username"]);
        $hashed_password = $site -> password_encryption($_POST["password"]);

        $query  = "INSERT INTO informe_admins (";
        $query .= "  username, hashed_password";
        $query .= ") VALUES (";
        $query .= "  '{$username}', '{$hashed_password}'";
        $query .= ")";
        $result = mysqli_query($connection, $query);

        if ($result) {
            // Success
            $_SESSION["message"] = "Admin {$username} has been created.";
            $site -> redirect_to("manage_admins.php");
        }
        else {
            // Failure
            $_SESSION["message"] = "Admin creation failed.";
        }
    }
}
else {
    // This is probably a GET request

} // end: if (isset($_POST['submit']))
?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php"); ?>
<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Admin menu</a>
        <br />

        <br />
        <a href="logout.php">Click here to log out</a>
    </div>
    <div id="page">
        <?php echo message(); ?>
        <?php echo $site -> form_errors($errors); ?>

        <h2>Create Admin</h2>
        <form action="new_admin.php" method="post">
            <p>Username:
                <input type="text" name="username" value="" />
            </p>
            <p>Password:
                <input type="password" name="password" value="" />
            </p>
            <input type="submit" name="submit" value="Create Admin" />
        </form>
        <br />
        <a href="manage_admins.php">Cancel</a>
    </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
