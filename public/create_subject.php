<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
if (isset($_POST['submit'])) {
	// Process the form

	$menu_name = $site -> mysql_prep($_POST["menu_name"]);
	$position = (int) $_POST["position"];
	$visible = (int) $_POST["visible"];
	
	// validations
	$required_fields = array("menu_name", "position", "visible");
	validate_presences($required_fields);
	
	$fields_with_max_lengths = array("menu_name" => 255);
	validate_max_lengths($fields_with_max_lengths);
	
	if (!empty($errors)) {
		$_SESSION["errors"] = $errors;
		$site -> redirect_to("new_subject.php");
	}
	
	$query  = "INSERT INTO informe_subjects (";
	$query .= "  menu_name, position, visible";
	$query .= ") VALUES (";
	$query .= "  '{$menu_name}', {$position}, {$visible}";
	$query .= ")";
	$result = mysqli_query($connection, $query);

	if ($result) {
		// Success
		$_SESSION["message"] = "Subject created.";
		$site -> redirect_to("manage_content.php");
	}
    else {
		// Failure
		$_SESSION["message"] = "Subject creation failed.";
		$site -> redirect_to("new_subject.php");
	}
	
}
else {
    // This is probably a GET request
    $site -> redirect_to("new_subject.php");
}
?>

<?php
	if (isset($connection)) { mysqli_close($connection); }
?>
