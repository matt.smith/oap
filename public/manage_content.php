<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php $layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>
<?php $pages -> find_selected_page($public_area = False); ?>

<div id="main">
  <div id="navigation">
      <br />
      <a href="admin.php">&laquo; Admin menu</a>
      <br />

      <?php echo $site -> navigation($current_subject, $current_page); ?>

      <br />
      <a href="new_subject.php">+ Add a subject</a>
      <br />

      <br />
      <a href="logout.php">Click here to log out</a>
  </div>
  <div id="page">
		<?php echo message(); ?>
		<?php if ($current_subject) { ?>
	    <h2>Manage Subject</h2>
			Menu name: <?php echo htmlentities($current_subject["menu_name"]); ?><br />
			Position: <?php echo $current_subject["position"]; ?><br />
			Visible: <?php echo $current_subject["visible"] == 1 ? 'yes' : 'no'; ?><br />
			<br />
			<a href="edit_subject.php?subject=<?php echo urlencode($current_subject["id"]); ?>">Edit Subject</a>
			
			<div style="margin-top: 2em; border-top: 1px solid #000000;">
				<h3>Pages in this subject:</h3>
				<ul>
				<?php
                $subject_pages = $subjects -> find_pages_for_subject($current_subject["id"], $public_area = False);
                while($page = mysqli_fetch_assoc($subject_pages)) {
                    echo "<li>";
                    $safe_page_id = urlencode($page["id"]);
                    echo "<a href=\"manage_content.php?page={$safe_page_id}\">";
                    echo htmlentities($page["menu_name"]);
                    echo "</a>";
                    echo "</li>";
                }
				?>
				</ul>
				<br />
				+ <a href="new_page.php?subject=<?php echo urlencode($current_subject["id"]); ?>">Add a new page to this subject</a>
			</div>

		<?php }
        elseif ($current_page) { ?>
			<h2>Manage Page</h2>
			Menu name: <?php echo htmlentities($current_page["menu_name"]); ?><br />
			Position: <?php echo $current_page["position"]; ?><br />
			Visible: <?php echo $current_page["visible"] == 1 ? 'yes' : 'no'; ?><br />
			Content:<br />
			<div class="view-content">
				<?php echo htmlentities($current_page["content"]); ?>
			</div>
			<br />
      <br />
      <a href="edit_page.php?page=<?php echo urlencode($current_page['id']); ?>">Edit page</a>
			
		<?php }
        else { ?>
			<h2>Manage Content</h2>
            Please select a subject or a page.
		<?php }?>
  </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
