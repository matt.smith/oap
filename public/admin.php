<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php $layout_context = "admin";    // alters the header, showing that user is in admin area in the page title. ?>
<?php include("../includes/layouts/header.php"); ?>

<div id="main">
  <div id="navigation">
      <br />
      <a href="index.php">&laquo; Main menu</a>
      <br />

      <br />
      <a href="logout.php">Click here to log out</a>
  </div>
  <div id="page">
    <h2>Admin Menu</h2>
    <p>Welcome to the admin area, <?php echo htmlentities($_SESSION["username"]); ?></p>
    <ul>
        <li><a href="manage_content.php">Manage Website Content</a></li>
        <li><a href="manage_admins.php">Manage Admin Users</a></li>
        <li><a href="manage_course_schedules.php">Manage Course Schedules</a></li>
    </ul>
  </div>
</div>

<?php include("../includes/layouts/footer.php"); ?>
