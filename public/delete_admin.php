<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// if user is not logged in as admin, then redirect them to login page so they can log in and gain access to the
// admin privileges.
$site -> logged_in_confirmation();
?>

<?php
$admin = $administrators -> find_admin_by_id($_GET["id"]);

if (!$admin) {
    // admin ID is missing, or it is invalid
    $site -> redirect_to("manage_admins.php");
}

$id = $admin["id"];
$query = "DELETE FROM informe_admins WHERE id = {$id} LIMIT 1";
$result = mysqli_query($connection, $query);

if ($result && mysqli_affected_rows($connection) == 1) {
    // Success
    $_SESSION["message"] = "Admin deleted.";
    $site -> redirect_to("manage_admins.php");
}
else {
    // Failure
    $_SESSION["message"] = "Admin deletion failed.";
    $site -> redirect_to("manage_admins.php");
}

?>
